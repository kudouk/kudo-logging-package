kudo logrus configuration for use in microservices

eg...
```
kudoLogWriter, kudoFieldHook, err := logger.Setup("order", "development")
if err != nil {
  logrus.WithError(err).Error("failed to init logging")
}
logrus.SetOutput(kudoLogWriter)
logrus.AddHook(kudoFieldHook)

logrus.WithError(errors.New("t")).Error("Transaction failed")
logrus.WithField("transaction", "some transaction").Debug("Transaction processed successfully")
log := logrus.WithFields(logrus.Fields{
  "user_id":        2384172,
  "transaction_id": 9823183,
  "amount":         5000,
  "currency":       "USD",
})
log.Info("Transaction processed successfully")
```