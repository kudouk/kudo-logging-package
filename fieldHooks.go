package logging

import (
	"os"

	"github.com/sirupsen/logrus"
)

type ExtraFieldHook struct {
	service     string
	environment string
	pid         int
}

func NewExtraFieldHook(service string, env string) *ExtraFieldHook {
	return &ExtraFieldHook{
		service:     service,
		environment: env,
		pid:         os.Getpid(),
	}
}
func (h *ExtraFieldHook) Levels() []logrus.Level {
	return logrus.AllLevels
}
func (h *ExtraFieldHook) Fire(entry *logrus.Entry) error {
	entry.Data["service"] = h.service
	entry.Data["environment"] = h.environment
	entry.Data["pid"] = h.pid
	return nil
}
