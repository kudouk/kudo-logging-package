package logging

import (
	"fmt"
	"runtime/debug"
	"time"

	errors "bitbucket.org/kudouk/error-package"
	pubsub "bitbucket.org/kudouk/kudo-pubsub-package"
	log "github.com/sirupsen/logrus"
)

type PubsubErrorLogger struct {
	startTime   time.Time
	logger      *log.Entry
	serviceType string
	messageType string
}

func NewPubsubErrorLogger(st pubsub.ServiceType, mt pubsub.MessageEventType) *PubsubErrorLogger {
	startTime := time.Now()

	logger := log.WithFields(log.Fields{
		"pubsub_service_type":       st,
		"pubsub_message_event_type": mt,
		"time_start":                startTime,
	})

	return &PubsubErrorLogger{
		startTime:   startTime,
		logger:      logger,
		serviceType: string(st),
		messageType: string(mt),
	}
}

// CapturePanic - for use within defer, recover
// defer func() {
// 	if r := recover(); r != nil {
// 	}
// }
func (el *PubsubErrorLogger) CapturePanic() {
	if r := recover(); r != nil {
		fmt.Println("Recover")
		elapsed := time.Since(el.startTime)

		el.logger.
			WithFields(log.Fields{
				"time_elapsed_ms": elapsed.Milliseconds(),
				"panic":           true,
				"stack":           string(debug.Stack()),
			}).
			Errorln(
				fmt.Errorf("%v, %v :: %v", el.serviceType, el.messageType, "panic"),
			)
	}
}

func (el *PubsubErrorLogger) LogError(err error) error {
	elapsed := time.Since(el.startTime)
	logger := el.logger.WithField("time_elapsed_ms", elapsed.Milliseconds())
	return errors.ProcessError(err, logger)
}
