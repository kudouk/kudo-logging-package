module bitbucket.org/kudouk/kudo-logging-package

go 1.12

require (
	bitbucket.org/kudouk/error-package v0.0.3
	bitbucket.org/kudouk/kudo-pubsub-package v0.0.20 // indirect
	github.com/getsentry/sentry-go v0.8.0
	github.com/makasim/sentryhook v0.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/objx v0.1.1 // indirect
	google.golang.org/grpc v1.42.0
)

// replace bitbucket.org/kudouk/error-package => ../error-package
