package logging

import (
	"regexp"
	"time"

	"github.com/getsentry/sentry-go"
	log "github.com/sirupsen/logrus"
)

type KudoLogger struct {
	Service   string
	Env       string
	SentryDSN string
}

func CreateKudoLogger(service string, env string, sentryDSN string) *KudoLogger {
	var err error
	levelRegex, err = regexp.Compile("level=([a-z]+)")
	if err != nil {
		log.WithError(err).Error("failed to init logging")
	}

	log.SetOutput(&LogWriter{})

	return &KudoLogger{
		Service:   service,
		Env:       env,
		SentryDSN: sentryDSN,
	}
}

func (kl *KudoLogger) AddSentryHook() func(timeout time.Duration) bool {
	newSentryHook := NewSentryHook(kl.SentryDSN, kl.Env)
	log.AddHook(newSentryHook)
	return sentry.Flush
}

func (kl *KudoLogger) AddFieldHook() {
	newExtraFieldHook := NewExtraFieldHook(kl.Service, kl.Env)
	log.AddHook(newExtraFieldHook)
}
