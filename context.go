package logging

import (
	"context"
)

type KudoCtxValue string

var ctxValue KudoCtxValue = "kudo_logger"

func AddPubsubLoggerToCtx(ctx context.Context, logger *PubsubErrorLogger) context.Context {
	return context.WithValue(ctx, ctxValue, logger)
}

func GetPubsubLoggerFromCtx(ctx context.Context) *PubsubErrorLogger {
	loggerInterface := ctx.Value(ctxValue)

	logger, ok := loggerInterface.(*PubsubErrorLogger)

	if !ok {
		return nil
	}

	return logger
}
