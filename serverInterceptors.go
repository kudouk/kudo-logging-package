package logging

import (
	"context"
	"fmt"
	"runtime/debug"
	"time"

	errors "bitbucket.org/kudouk/error-package"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

// UnaryServerInterceptor returns a new unary server interceptors that adds logrus.Entry to the context.
func UnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		startTime := time.Now()

		logger := log.WithFields(log.Fields{
			"grpc_method": info.FullMethod,
			"time_start":  startTime,
		})

		defer func() {
			if r := recover(); r != nil {
				elapsed := time.Since(startTime)

				logger.
					WithFields(log.Fields{
						"time_elapsed_ms": elapsed.Milliseconds(),
						"panic":           true,
						"stack":           string(debug.Stack()),
					}).
					Errorln(
						fmt.Errorf("%v :: %v", info.FullMethod, "panic"),
					)
			}
		}()

		resp, err := handler(ctx, req)

		if err != nil {
			elapsed := time.Since(startTime)
			logger = logger.WithField("time_elapsed_ms", elapsed.Milliseconds())
			return resp, errors.ProcessError(err, logger)
		}

		return resp, err
	}
}
