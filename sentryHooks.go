package logging

import (
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/sirupsen/logrus"

	"github.com/makasim/sentryhook"
)

func NewSentryHook(sentryDSN string, env string) sentryhook.Hook {
	options := sentry.ClientOptions{
		Dsn:         "https://68eb829ac66d48f78e4287403c165339@o484760.ingest.sentry.io/5538470",
		Environment: env,
	}
	if err := sentry.Init(options); err != nil {
		logrus.WithError(err).Error("failed to init sentry")
	}
	defer sentry.Flush(2 * time.Second)

	return sentryhook.New([]logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel})
}
